//
//  ViewController.h
//  App 1.0
//
//  Created by Martin Gábor on 23.4.2014.
//  Copyright (c) 2014 Martin Gábor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property (strong, nonatomic) IBOutlet UITextField *textFieldOne;
@property (strong, nonatomic) IBOutlet UITextField *textFieldTwo;

@property (strong, nonatomic) IBOutlet UILabel *sideOne;
@property (strong, nonatomic) IBOutlet UILabel *sideTwo;

@property (strong, nonatomic) IBOutlet UILabel *unknownSide;



@end
