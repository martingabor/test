//
//  AppDelegate.h
//  App 1.0
//
//  Created by Martin Gábor on 23.4.2014.
//  Copyright (c) 2014 Martin Gábor. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
