//
//  ViewController.m
//  App 1.0
//
//  Created by Martin Gábor on 23.4.2014.
//  Copyright (c) 2014 Martin Gábor. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [_segmentedControl titleForSegmentAtIndex:(0)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
