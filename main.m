//
//  main.m
//  App 1.0
//
//  Created by Martin Gábor on 23.4.2014.
//  Copyright (c) 2014 Martin Gábor. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    //anakonda honda 10
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
